<?php

namespace CreditLineEngine;

use CreditLineEngine\Entities\CLOrderStatus;
use CreditLineEngine\Entities\CLRequest;
use CreditLineEngine\Entities\CLResponse;

/**
 * Контракт класса CreditLine (методы службы)
 */
interface ICreditLine
{
    /**
     * Проверяет данные аутентификации
     * @return bool
     */
    public function ConfirmOrganization();

    /**
     * Отправляет заявку на кредит
     * @param CLRequest $request Заявка на кредит
     * @return CLResponse
     */
    public function ProcessCreditLineApplication($request);

    /**
     * Возвращает статус заказа
     * @param $orderId Номер заказа
     * @return Entities\CLOrderStatus
     */
    public function GetOrderStatus($orderId);

    /**
     * Возвращает отчет по заказам
     * @param $startDate Начальная дата
     * @param $endDate Конечная дата
     * @return Entities\OrderReport
     */
    public function GetOrderReport($startDate, $endDate);
}