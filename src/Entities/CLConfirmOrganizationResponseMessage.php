<?php

namespace CreditLineEngine\Entities;

/**
 * Ответное сообщение на запрос проверки организации в системе CreditLine
 * @package CreditLineEngine\Entities
 */
class CLConfirmOrganizationResponseMessage
{
    /**
     * @var bool Активна ли точка (в случае ошибки возвращает false)
     */
    public $IsActive;
}