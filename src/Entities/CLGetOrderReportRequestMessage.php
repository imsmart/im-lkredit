<?php

namespace CreditLineEngine\Entities;

include_once("Dates.php");

/**
 * Сообщение получения отчета по заказам за определенный период от сервиса CreditLine
 * @package CreditLineEngine\Entities
 */
class CLGetOrderReportRequestMessage
{
    /**
     * @var Dates Диапазон дат для отчета
     */
    public $GetOrderDates;

    /**
     * Создает объект класса
     */
    public function __construct()
    {
        $this->GetOrderDates = new Dates();
    }
}