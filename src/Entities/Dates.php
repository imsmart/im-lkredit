<?php

namespace CreditLineEngine\Entities;

/**
 * Диапазон дат для отчета
 * @package CreditLineEngine\Entities
 */
class Dates
{
    /**
     * @var string Начальная дата
     */
    public $StartDate;

    /**
     * @var string Конечная дата
     */
    public $EndDate;
}