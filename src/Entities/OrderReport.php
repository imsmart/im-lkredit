<?php

namespace CreditLineEngine\Entities;

include_once("CLResponse.php");

/**
 * Отчет
 * @package CreditLineEngine\Entities
 */
class OrderReport extends CLResponse
{
    /**
     * @var string Отчет в XML
     */
    public $Report;
}