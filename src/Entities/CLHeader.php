<?php

namespace CreditLineEngine\Entities;

/**
 * Заголовок SOAP запроса
 * @package CreditLineEngine\Entities
 */
class CLHeader
{
    /**
     * @var string Хеш-функция логина Партнера
     */
    public $PartnerLogin;

    /**
     * @var string Хеш-функция пароля Партнера
     */
    public $PartnerPassword;
}