<?php

namespace CreditLineEngine\Entities;

include_once("OrderReport.php");

/**
 * Ответное сообщение на запрос получения отчета по заказам за определенный период от сервиса CreditLine
 * @package CreditLineEngine\Entities
 */
class CLGetOrderReportResponseMessage
{
    /**
     * @var OrderReport Отчет
     */
    public $Result;

    /**
     * Создает объект класса
     */
    public function __construct()
    {
        $this->Result = new OrderReport();
    }
}