<?php

namespace CreditLineEngine\Entities;

/**
 * Сообщение получения статуса заказа в системе CreditLine
 * @package CreditLineEngine\Entities
 */
class CLGetOrderStatusRequestMessage
{
    /**
     * @var string Номер заказа в системе Партнера
     */
    public $NumOrder;

    /**
     * Создает объект класса
     * @param $numOrder Номер заказа
     */
    public function __construct($numOrder)
    {
        $this->NumOrder = $numOrder;
    }
}