<?php

namespace CreditLineEngine\Entities;

include_once("BanksEnum.php");
include_once("Good.php");
include_once("CreditPreferences.php");

/**
 * Информация о кредите
 * @package CreditLineEngine\Entities
 */
class Credit
{
    /**
     * @var float Размер скидки. Если скидка не указывается, то можно указывать 0.
     */
    public $Discount;

    /**
     * @var float Сумма покупки
     */
    public $CreditSum;

    /**
     * @var Good[] Список товаров
     */
    public $Goods;

    /**
     * @var CreditPreferences Предпочтения клиента по кредиту
     */
    public $Preference;

    /**
     * Создает объект класса
     * @param float $discount Размер скидки. Если скидка не указывается, то можно указывать 0.
     * @param float $creditSum Сумма покупки
     * @param Good[] $goods Список товаров
     * @param $creditPreference Предпочтения клиента по кредиту
     */
    public function __construct($discount, $creditSum, $goods, $creditPreference)
    {
        $this->Discount = $discount;
        $this->CreditSum = $creditSum;
        $this->Goods = $goods;
        $this->Preference = $creditPreference;
    }
}