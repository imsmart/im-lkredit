<?php

namespace CreditLineEngine\Entities;

include_once("CLResponse.php");

/**
 * Статус заказа
 * @package CreditLineEngine\Entities
 */
class CLOrderStatus extends CLResponse
{
    /**
     * @var string Статус
     */
    public $Status;

    /**
     * @var string Номер заказа в системе Партнера
     */
    public $NumOrder;

    /**
     * Получить описание статуса
     */
    public function GetStatusDescription()
    {
        switch($this->Status)
        {
            case "Refused":
                return "Отказ";
            case "Accepted":
                return "Одобрение";
            case "Annuled":
                return "Аннулирован";
            case "Incompleted":
                return "Incompleted";
            case "InWork":
                return "В работе";
            case "InStack":
                return "В очереди на обработку";
            case "Cashed":
                return "Оплачен";
            case "InCash":
                return "В процессе оплаты";
            case "Signed":
                return "Подписан";
            default:
                return "";
        }
    }
}