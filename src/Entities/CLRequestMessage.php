<?php

namespace CreditLineEngine\Entities;
include_once("CLRequest.php");

/**
 * Сообщение заявки на кредит
 * @package CreditLineEngine\Entities
 */
class CLRequestMessage
{
    /**
     * @var CLRequest Тело заявки (параметры заявки на кредит)
     */
    public $CreditLineRequest;

    /**
     * Создает объект класса
     */
    public function __construct()
    {
        $this->CreditLineRequest = new CLRequest();
    }
}