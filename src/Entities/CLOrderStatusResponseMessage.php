<?php

namespace CreditLineEngine\Entities;

include_once("CLOrderStatus.php");

/**
 * Ответное сообщение на запрос проверки статуса заказа в системе CreditLine
 * @package CreditLineEngine\Entities
 */
class CLOrderStatusResponseMessage
{
    /**
     * @var CLOrderStatus Тело ответа
     */
    public $CreditLineResponse;

    /**
     * Создает объект класса
     */
    public function __construct()
    {
        $this->CreditLineResponse = new CLOrderStatus();
    }
}