<?php

namespace CreditLineEngine\Entities;

/**
 * Ответ заявки на кредит
 * @package CreditLineEngine\Entities
 */
class CLResponse
{
    /**
     * @var bool Статус выполнения операции
     */
    public $Confirm;

    /**
     * @var integer Код ошибки (если ошибки нет, то возвращается 0)
     */
    public $ErrorCode;

    /**
     * @var string Текст ошибки
     */
    public $ErrorText;
}