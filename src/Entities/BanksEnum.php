<?php

namespace CreditLineEngine\Entities;

/**
 * Перечисление банков
 * @package CreditLineEngine\Entities
 */
abstract class BanksEnum
{
    const None = "";
    const HomeCreditAndFinanceBank = "HomeCreditAndFinanceBank";
    const BankRussianStandard = "BankRussianStandard";
    const CreditEuropeBank = "CreditEuropeBank";
    const OTPBank = "OTPBank";
    const RenessansCreditBank = "RenessansCreditBank";
    const AlfaBank = "AlfaBank";
    const SetelemBank = "SetelemBank";
}