<?php

namespace CreditLineEngine\Entities;
include_once("CLResponse.php");

/**
 * Сообщение ответа заявки на кредит
 * @package CreditLineEngine\Entities
 */
class CLResponseMessage
{
    /**
     * @var CLResponse Тело ответа
     */
    public $CreditLineResponse;
}