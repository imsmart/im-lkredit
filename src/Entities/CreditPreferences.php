<?php

namespace CreditLineEngine\Entities;

include_once("BanksEnum.php");

/**
 * Предпочтения клиента по кредиту
 * @package CreditLineEngine\Entities
 */
class CreditPreferences
{
    /**
     * @var float Предполагаемый первоначальный платеж
     */
    public $InitialPayment;

    /**
     * @var integer Предполагаемый срок кредита
     */
    public $CreditPeriod;

    /**
     * @var string Предполагаемый банк кредитования
     */
    public $Bank;

    /**
     * @var string Предполагаемая акция (кредитный продукт)
     */
    public $Action;

    /**
     * Создает объект класса
     * @param float $initialPayment Предполагаемый первоначальный платеж
     * @param int $creditPeriod Предполагаемый срок кредита
     * @param string $bank Банк
     * @param string $action Предполагаемая акция (кредитный продукт)
     */
    public function __construct($initialPayment = .0, $creditPeriod = 0, $bank = BanksEnum::None, $action = "")
    {
        $this->InitialPayment = $initialPayment;
        $this->CreditPeriod = $creditPeriod;
        if(!empty($bank))
        {
            $this->Bank = $bank;
        }
        $this->Action = $action;
    }
}