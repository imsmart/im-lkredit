<?php

namespace CreditLineEngine\Entities;

/**
 * Информация о клиенте
 * @package CreditLineEngine\Entities
 */
class Client
{
    /**
     * @var string Имя клиента
     */
    public $ClientFirstName;

    /**
     * @var string Фамилия клиента
     */
    public $ClientLastName;

    /**
     * @var string Отчество клиента
     */
    public $ClientMiddleName;

    /**
     * @var string Контактный телефон клиента
     */
    public $ClientContactPhone;

    /**
     * @var string Дополнительный контактный телефон клиента
     */
    public $ClientExtendedContactPhone;

    /**
     * @var string Адрес электронной почты клиента
     */
    public $ClientEmail;

    /**
     * @var string Дата рождения клиента
     */
    public $ClientBirthDate;

    /**
     * Создает объект класса
     * @param string $phone Номер телефона
     * @param string $lastName Фамилия
     * @param string $firstName Имя
     * @param string $middleName Отчество
     * @param string $email Адрес электронной почты
     * @param string $birthDate Дата рождения
     * @param string $extendedPhone Дополнительный контактный телефон клиента
     */
    public function __construct($phone, $lastName = "", $firstName = "", $middleName = "", $email = "", $birthDate = "", $extendedPhone = "")
    {
        $this->ClientContactPhone = $phone;
        $this->ClientLastName = $lastName;
        $this->ClientFirstName = $firstName;
        $this->ClientMiddleName = $middleName;
        $this->ClientEmail = $email;
        $this->ClientExtendedContactPhone = $extendedPhone;
        if(!empty($birthDate))
        {
            $this->ClientBirthDate = $birthDate;
        }
    }
}