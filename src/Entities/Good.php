<?php

namespace CreditLineEngine\Entities;

/**
 * Товар
 * @package CreditLineEngine\Entities
 */
class Good
{
    /**
     * @var string Наименование товара
     */
    public $GoodName;

    /**
     * @var float Цена за единицу товара
     */
    public $GoodPrice;

    /**
     * @var integer Количество единиц товара
     */
    public $GoodCount;

    /**
     * Создает объект класса
     * @param string $name Наименование товара
     * @param float $price Цена за единицу товара
     * @param integer $qty Количество единиц товара
     */
    public function __construct($name, $price, $qty)
    {
        $this->GoodName = $name;
        $this->GoodPrice = $price;
        $this->GoodCount = $qty;
    }

    /**
     * Возвращает полную сумму группы товаров
     * @return float Сумма товаров
     */
    public function GetTotalPrice()
    {
        return $this->GoodPrice * $this->GoodCount;
    }
}