<?php

namespace CreditLineEngine\Entities;
include_once("Client.php");
include_once("Credit.php");

/**
 * Заявка на кредит
 * @package CreditLineEngine\Entities
 */
class CLRequest
{
    /**
     * @var string Номер заказа в системе Партнёра
     */
    public $NumOrder;

    /**
     * @var Client Информация о клиенте
     */
    public $Client;

    /**
     * @var Credit Информация о кредите
     */
    public $Credit;

    /**
     * @var string Наименование магазина
     */
    public $ShopName;

    /**
     * @var string Наличие товара на складе
     */
    public $GoodsInStore;

    /**
     * @var string Чьими силами производится подписание КД
     */
    public $SigningKD;

    /**
     * @var string Удобное время для звонка
     */
    public $CallTime;
}