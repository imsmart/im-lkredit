<?php

namespace CreditLineEngine;

use CreditLineEngine\Entities\BanksEnum;
use CreditLineEngine\Entities\CLConfirmOrganizationRequestMessage;
use CreditLineEngine\Entities\CLConfirmOrganizationResponseMessage;
use CreditLineEngine\Entities\CLGetOrderReportRequestMessage;
use CreditLineEngine\Entities\CLGetOrderReportResponseMessage;
use CreditLineEngine\Entities\CLGetOrderStatusRequestMessage;
use CreditLineEngine\Entities\Client;
use CreditLineEngine\Entities\CLOrderStatusResponseMessage;
use CreditLineEngine\Entities\CLRequest;
use CreditLineEngine\Entities\CLRequestMessage;
use CreditLineEngine\Entities\CLResponse;
use CreditLineEngine\Entities\CLResponseMessage;
use CreditLineEngine\Entities\Credit;
use CreditLineEngine\Entities\CreditPreferences;
use CreditLineEngine\Entities\Good;
use Exception;

// include_once("ICreditLine.interface.php");
// include_once("CreditLineServicesClient.class.php");
// include_once("OrderReport.class.php");

// include_once("Entities/Good.class.php");
// include_once("Entities/Client.class.php");
// include_once("Entities/Credit.class.php");

// include_once("Entities/CLRequestMessage.class.php");
// include_once("Entities/CLResponseMessage.class.php");

// include_once("Entities/CLRequest.class.php");
// include_once("Entities/CLResponse.class.php");

// include_once("Entities/CLOrderStatusResponseMessage.class.php");
// include_once("Entities/CLGetOrderStatusRequestMessage.class.php");

// include_once("Entities/CLConfirmOrganizationRequestMessage.class.php");
// include_once("Entities/CLConfirmOrganizationResponseMessage.class.php");

// include_once("Entities/CLGetOrderReportRequestMessage.class.php");
// include_once("Entities/CLGetOrderReportResponseMessage.class.php");

/**
 * Класс для работы с Web-службой CreditLine
 * @package CreditLine
 */
class CreditLine implements ICreditLine
{
    #region Fields
    /**
     * @var float Время начала выполнения операции
     */
    private $startTime;

    /**
     * @var float Время конца выполнения операции
     */
    private $endTime;

    /**
     * @var string Адрес службы CreditLine
     */
    private $host;

    /**
     * @var string Логин для доступа к службе CreditLine
     */
    private $login;

    /**
     * @var string Пароль для доступа к службе CreditLine
     */
    private $password;

    /**
     * @var CreditLineServicesClient Клиент для сервисов CreditLine
     */
    private $clClient;

    #endregion

    #region Constructors
    /**
     * Создает объект для доступа к методам службы CreditLine
     * @param string $host Адрес службы
     * @param string $login Логин
     * @param string $password Пароль
     * @throws Exception Исключение
     */
    public function __construct($host, $login, $password)
    {
        if (!$this->ExtensionIncluded("soap")) {
            throw new Exception("Критическая ошибка, модуль SOAP не подключен");
        } else if (!$this->ExtensionIncluded("openssl")) {
            throw new Exception("Критическая ошибка, модуль OpenSSL не подключен");
        } else if (empty($host) || empty($login) || empty($password)) {
            throw new Exception("Не корректные параметры для подключения к сервису");
        }

        $this->host = $host;
        $this->login = $login;
        $this->password = $password;

        $this->clClient = new CreditLineServicesClient($this->host, $this->login, $this->password);
    }

    #endregion

    #region Public methods
    /**
     * Проверяет данные аутентификации
     * @return bool Ответ
     */
    public function ConfirmOrganization()
    {
        $this->FixStartTime();

        $request = new CLConfirmOrganizationRequestMessage();
        $response = new CLConfirmOrganizationResponseMessage();
        $this->clClient->Call(__FUNCTION__, $request, $response);

        $this->FixEndTime();

        return $response->IsActive;
    }

    /**
     * Отправляет заявку на кредит
     * @param CLRequest $request Заявка на кредит
     * @return CLResponse
     */
    public function ProcessCreditLineApplication($request)
    {
        $this->FixStartTime();

        $requestMessage = new CLRequestMessage();
        $requestMessage->CreditLineRequest = $request;

        $responseMessage = new CLResponseMessage();
        $this->clClient->Call(__FUNCTION__, $requestMessage, $responseMessage);

        $this->FixEndTime();

        return $responseMessage->CreditLineResponse;
    }

    /**
     * Возвращает статус заказа
     * @param string $orderId Номер заказа
     * @return Entities\CLOrderStatus
     */
    public function GetOrderStatus($orderId)
    {
        $this->FixStartTime();

        $request = new CLGetOrderStatusRequestMessage($orderId);
        $response = new CLOrderStatusResponseMessage();
        $this->clClient->Call(__FUNCTION__, $request, $response);

        $this->FixEndTime();

        return $response->CreditLineResponse;
    }

    /**
     * Возвращает отчет по заказам
     * @param $startDate Начальная дата
     * @param $endDate Конечная дата
     * @return Entities\OrderReport
     */
    public function GetOrderReport($startDate, $endDate)
    {
        $this->FixStartTime();

        $request = new CLGetOrderReportRequestMessage();
        $request->GetOrderDates->StartDate = $startDate;
        $request->GetOrderDates->EndDate = $endDate;

        $response = new CLGetOrderReportResponseMessage();
        $this->clClient->Call(__FUNCTION__, $request, $response);

        $this->FixEndTime();
        return $this->ConvertXMLReport($response->Result->Report->any);
    }

    /**
     * Возвращает заявку на кредит
     * @param string $orderId Номер заказа
     * @param Client $client Информация о клиенте
     * @param Credit $credit Информация о кредите
     * @param string $callTime Удобное время для звонка
     * @param string $shopName Наименование магазина
     * @param string $goodsInStore Наличие товара на складе
     * @param string $signingKD Чьими силами производится подписание КД
     * @return CLRequest
     */
    public static function CreateCLRequest($orderId, $client, $credit, $callTime = "", $shopName = "", $goodsInStore = "", $signingKD = "")
    {
        $clRequest = new CLRequest();
        $clRequest->Client = $client;
        $clRequest->Credit = $credit;
        $clRequest->GoodsInStore = $goodsInStore;
        $clRequest->NumOrder = $orderId;
        $clRequest->ShopName = $shopName;
        $clRequest->SigningKD = $signingKD;
        $clRequest->CallTime = $callTime;
        return $clRequest;
    }

    /**
     * Создает товар
     * @param string $name Наименование
     * @param float $price Цена
     * @param integer $qty Количество
     * @return array
     */
    public static function CreateGood($name, $price, $qty)
    {
        $good = array();
        $good["Name"] = $name;
        $good["Price"] = $price;
        $good["Count"] = $qty;
        return $good;
    }

    /**
     * Создает клиента
     * @param string $phone Номер телефона
     * @param string $name Имя
     * @param string $email Адрес электронной почты
     * @param string $extendedPhone Дополнительный контактный телефон
     * @return Client
     */
    public static function CreateClient($phone, $name, $email, $extendedPhone = "")
    {
        return new Client($phone, $name, "", "", $email, "", $extendedPhone);
    }

    /**
     * Создает параметры кредита
     * @param Good[] $goodsArray Список товаров
     * @param float $initialPayment Предполагаемый первоначальный платеж
     * @param integer $creditPeriod Предполагаемый срок кредита
     * @param float $discount Размер скидки. Если скидка не указывается, то можно указывать 0.
     * @param string $bank Предполагаемый банк кредитования
     * @param string $action Предполагаемая акция (кредитный продукт)
     * @return Credit
     */
    public static function CreateCredit($goodsArray, $initialPayment = .0, $creditPeriod = 0, $discount = .0, $bank = BanksEnum::None, $action = "")
    {
        $creditSum = .0;
        $goods = array();
        foreach ($goodsArray as $goodValue) {
            $good = new Good($goodValue["Name"], $goodValue["Price"], $goodValue["Count"]);
            array_push($goods, $good);
            $creditSum += $good->GetTotalPrice();
        }
        return new Credit($discount, $creditSum, $goods, new CreditPreferences($initialPayment, $creditPeriod, $bank, $action));
    }

    #endregion

    #region Private methods
    private function StructToArray($values, &$i)
    {
        $child = array();
        if (isset($values[$i]['value'])) array_push($child, $values[$i]['value']);
        while ($i++ < count($values)) {
            switch ($values[$i]['type']) {
                case 'cdata':
                    array_push($child, $values[$i]['value']);
                    break;
                case 'complete':
                    $name = $values[$i]['tag'];
                    if (!empty($name)) {
                        $child[$name] = ($values[$i]['value']) ? ($values[$i]['value']) : '';
                        if (isset($values[$i]['attributes'])) {
                            $child[$name] = $values[$i]['attributes'];
                        }
                    }
                    break;
                case 'open':
                    $name = $values[$i]['tag'];
                    $size = isset($child[$name]) ? sizeof($child[$name]) : 0;
                    $child[$name][$size] = $this->StructToArray($values, $i);
                    break;
                case 'close':
                    return $child;
                    break;
            }
        }
        return $child;
    }

    private function ConvertXMLReport($xml)
    {
        $secondPartPos = strpos($xml, "<diffgr");
        $xml = substr($xml, $secondPartPos);

        $values = array();
        $tags = array();
        $array = array();

        $parser = xml_parser_create();
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, $xml, $values, $tags);
        xml_parser_free($parser);

        $skipFirst = true;
        foreach ($tags as $key => $value) {
            if ($key != "Result") {
                continue;
            }
            // каждая пара вхождений массива это нижняя и верхняя
            // границы диапазона для определения
            for ($i = 0; $i < count($value); $i += 2) {
                if ($skipFirst) {
                    $skipFirst = false;
                    continue;
                }
                $offset = $value[$i] + 1;
                $len = $value[$i + 1] - $offset;
                $slicedArray = array_slice($values, $offset, $len);
                $array[] = $this->ParseArray($slicedArray);
            }
        }
        return $array;
    }

    private function ParseArray($values)
    {
        $report = array();
        for ($i = 0; $i < count($values); $i++) {
            $value = NULL;
            if (isset($values[$i]["value"])) {
                $value = $values[$i]["value"];
            }
            $report[$values[$i]["tag"]] = $value;
        }
        return new OrderReport($report);
    }

    /**
     * Определяет, загружено ли PHP расширение
     * @param string $extensionName Имя расширения
     * @return bool Результат проверки
     */
    private function ExtensionIncluded($extensionName)
    {
        return in_array($extensionName, get_loaded_extensions());
    }

    /**
     * Фиксирует время начала выполнения операции
     */
    private function FixStartTime()
    {
        $this->startTime = microtime(true);
    }

    /**
     * Фиксирует время конца выполнения операции
     */
    private function FixEndTime()
    {
        $this->endTime = microtime(true);
    }

    /**
     * Возвращает время исполнения операции
     * @return float Время исполнения операции
     */
    public function GetRunTime()
    {
        return round($this->endTime - $this->startTime, 4);
    }
    #endregion
}