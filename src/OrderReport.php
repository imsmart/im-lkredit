<?php

namespace CreditLineEngine;

/**
 * Отчет
 * @package CreditLineEngine
 */
class OrderReport
{
    /**
     * @var string ФИО клиента
     */
    public $ClientName;

    /**
     * @var float ПВ
     */
    public $InitialPayment;

    /**
     * @var string Номер заказа
     */
    public $OrderId;

    /**
     * @var string Дата заявки
     */
    public $ApplicationDate;

    /**
     * @var string Статус заявки
     */
    public $Status;

    public function __construct($array)
    {
        foreach ($array as $key => $value)
        {
            if(is_null($value))
            {
                continue;
            }
            switch($key)
            {
                case "ClientFIO":
                    $this->ClientName = $value;
                    break;
                case "InitialPayment":
                    $this->InitialPayment = $value;
                    break;
                case "OrderID":
                    $this->OrderId = $value;
                    break;
                case "AppDT":
                    $this->ApplicationDate = $value;
                    break;
                case "Status":
                    $this->Status = $value;
                    break;
            }
        }
    }
}