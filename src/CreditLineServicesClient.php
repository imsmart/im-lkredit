<?php

namespace CreditLineEngine;

use Exception;
use SoapClient;
use SoapFault;
use SoapHeader;
use stdClass;

/**
 * Класс для представления доступа к сервисам CreditLine
 * @package CreditLine
 */
class CreditLineServicesClient
{
    /**
     * @var SoapClient Клиент для сервисов SOAP
     */
    private $soapClient;

    public function __construct($host, $login, $password)
    {
        ini_set("soap.wsdl_cache_enabled", "0");
        $this->soapClient = new SoapClient($host);
        //$this->soapClient = new SoapClient($host, array("trace" => 1));
        $auth = array
        (
            "PartnerLogin" => md5($login),
            "PartnerPassword" => md5($password),
        );

        $header = new SoapHeader("http://tempuri.org/", 'CreditLineHeader', $auth);
        $this->soapClient->__setSoapHeaders($header);
    }

    /**
     * Отправляет запрос службе
     * @param $methodName Имя метода службы
     * @param $request Объект запроса
     * @param $response Ответ
     */
    public function Call($methodName, $request, &$response)
    {
        $soapResult = NULL;
        try {
            $soapResult = $this->soapClient->__call($methodName, array($request));
        } catch (SoapFault $fault) {
            $this->HandleSoapException($fault);
        }

        $this::Cast($response, $soapResult);
    }

    /**
     * Обрабатывает SOAP исключение
     * @param SoapFault $soapFault
     * @throws Exception Исключение
     */
    private function HandleSoapException($soapFault)
    {
        throw $soapFault;
    }

    /**
     * Переводит тип из stdClass в пользовательский тип
     * @param $destination Объект назначения
     * @param stdClass $source Источник
     */
    private static function Cast(&$destination, stdClass $source)
    {
        $sourceReflection = new \ReflectionObject($source);
        $sourceProperties = $sourceReflection->getProperties();
        foreach ($sourceProperties as $sourceProperty) {
            $name = $sourceProperty->getName();
            if (gettype($destination->{$name}) == "object") {
                self::Cast($destination->{$name}, $source->$name);
            } else {
                $destination->{$name} = $source->$name;
            }
        }
    }
}