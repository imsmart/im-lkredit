<?php

/**
 * Пример проверки данных аутентификации
 */

/* Подключаем класс CreditLine */
include("../CreditLine.php");
use CreditLineEngine\CreditLine;

/* Данные для аутентификации на Web-службе */
$wsdl = "https://s1.l-kredit.ru/InternetShopCreditLineWork/ISCL.svc?wsdl";
$login = "test";
$password = "test";

/* Создаем клиента CreditLine */
$clClient = new CreditLine($wsdl, $login, $password);

/* Проверяем данные аутентификации */
$result = $clClient->ConfirmOrganization();
if($result)
{
    // Данные аутентификации верны
}
else
{
    // Ошибка аутентификации
}