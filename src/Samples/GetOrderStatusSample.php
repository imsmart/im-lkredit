<?php

/**
 * Пример получения статуса заявки
 */

/* Подключаем класс CreditLine */
include("../CreditLine.php");
use CreditLineEngine\CreditLine;

/* Данные для аутентификации на Web-службе */
$wsdl = "https://s1.l-kredit.ru/InternetShopCreditLineWork/ISCL.svc?wsdl";
$login = "test";
$password = "test";

/* Создаем клиента CreditLine */
$clClient = new CreditLine($wsdl, $login, $password);

// Известный номер заявки (номер заказа в магазине)
$orderId = "QQ1";
// Выполнение запроса
$result = $clClient->GetOrderStatus($orderId);

if($result->Confirm)
{
    $status = $result->Status; // Статус заявки
    $description = $result->GetStatusDescription(); // Описание статуса заявки
}
else
{
    // Ошибка получения статуса
    $errCode = $result->ErrorCode; // Код ошибки
    $errText = $result->ErrorText; // Текст ошибки
}