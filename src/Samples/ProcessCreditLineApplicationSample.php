<?php

/**
 * Пример отправки заявки на кредит
 */

/* Подключаем класс CreditLine */
include("../CreditLine.php");
use CreditLineEngine\CreditLine;

/* Данные для аутентификации на Web-службе */
$wsdl = "https://s1.l-kredit.ru/InternetShopCreditLineWork/ISCL.svc?wsdl";
$login = "test";
$password = "test";

/* Создаем клиента CreditLine */
$clClient = new CreditLine($wsdl, $login, $password);

/* Создает заявку на кредит */
$g1 = CreditLine::CreateGood("Телефон", 100.50, 10); // Создаем товар, 10 телефонов за 100.50
$g2 = CreditLine::CreateGood("Часы", 120, 2); // Создаем еще товар
$goods = array($g1, $g2); // Заносим товары в массив

// Создает объект клиента с номером телефона, именем, почтой
// так же можно указать необязательное поле - дополнительный номер телефона
// $client = CreditLine::CreateClient("+79008001122", "Иванов Иван Иванович", "ivan@ivanov.ru", "+79008001133");
$client = CreditLine::CreateClient("+79008001122", "Иванов Иван Иванович", "ivan@ivanov.ru");

// Создает объект кредита с массивом товаров,
// опциональным первоначальным взносом 1200
// и сроком на 12 месяцев.
// Можно создать и такой объект (без взноса и срока, только с товарами)
// $credit = CreditLine::CreateCredit($goods);
$credit = CreditLine::CreateCredit($goods, 1200, 12);

// Создает заявку на кредит с номером "CoolOrderNumber123",
// ранее созданными объектами клиента и кредита.
// Существуют и другие необязательные параметры (см. описание метода)
$request = CreditLine::CreateCLRequest("CoolOrderNumber123", $client, $credit);

// Отправка запроса с ранее созданным объектом заявки
$result = $clClient->ProcessCreditLineApplication($request);

if($result->Confirm)
{
    // Заявка успешно отправлена
}
else
{
    // Ошибка отправки заявки
    $errCode = $result->ErrorCode; // Код ошибки
    $errText = $result->ErrorText; // Текст ошибки
}